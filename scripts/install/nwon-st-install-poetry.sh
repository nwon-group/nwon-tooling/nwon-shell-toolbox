#!/bin/bash
set -euo pipefail

curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python3

echo "

# Poetry - Python Package Manager
export PATH=\"$HOME/.poetry/bin:${PATH}\"" >>~/.zshenv

# Oh-My-Zsh
mkdir $ZSH/plugins/poetry
poetry completions zsh >$ZSH/plugins/poetry/_poetry
