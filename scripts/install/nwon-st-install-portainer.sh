#!/bin/bash
set -euo pipefail

# create volume
docker volume create portainer_data

# pull latest version of the image
docker pull portainer/portainer-ce

# clear existing container
docker stop portainer
docker rm portainer

# create container that is always restarting
docker run -d \
    -p 8000:8000 -p 9000:9000 \
    --name=portainer \
    --restart=always \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -v portainer_data:/data \
    portainer/portainer-ce
