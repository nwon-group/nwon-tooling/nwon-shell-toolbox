#!/bin/bash
set -euo pipefail

source $(dirname $(dirname "${BASH_SOURCE[0]}"))/nwon-se-utils.sh

userHasSudoRights

SWAP_FILE_SIZE_IN_GB=${1:-1}

logInfo "Creating a swap file of ${SWAP_FILE_SIZE_IN_GB}gb"

# according to https://www.digitalocean.com/community/tutorials/how-to-add-swap-space-on-ubuntu-18-04
sudo fallocate -l ${SWAP_FILE_SIZE_IN_GB}G /swapfile
sudo chmod 600 /swapfile
sudo mkswap /swapfile
sudo swapon /swapfile

# making the swapfile permanent
echo '/swapfile none swap sw 0 0' | sudo tee -a /etc/fstab

# not sure about the following to things
sudo sysctl vm.vfs_cache_pressure=50
sudo sysctl vm.swappiness=10
