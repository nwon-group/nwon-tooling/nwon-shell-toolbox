#!/bin/bash
set -euo pipefail

#####################################################
# This is basically a step by step guide on how
# to setup a remote system for NVON
#####################################################

sudo echo "I am root" >/dev/null || (print "Sudo rights required to run this script" && exit 1)

# install docker as we nearly always need it
apt-get install docker --assume-yes
groupadd docker

# adding user nvon (will ask for password)
# please store in bitwarden
adduser nvon
usermod -aG sudo nvon
usermod -aG docker nvon

# change to user nvon
su nvon

# set git config
git config --global user.email "remote-server@nwon.de"
git config --global user.name "NVON - Remote Server"

# create ssh key and create file for authorized keys
ssh-keygen
touch /home/nvon/.ssh/authorized_keys
chmod 600 /home/nvon/.ssh/authorized_keys

# set hostname
sudo hostnamectl set-hostname nvon-testing

# Clone nvon shell environment
# First you have to grant read permission to the generated ssh key
# Display key via
# cat ~/.ssh/id_rsa.pub
git clone git@gitlab.com:nvon/nvon-shell-environment.git /home/nvon/nvon-shell-environment

# run install script for nvon shell environment
/home/nvon/nvon-shell-environment/scripts/nwon-se-install.sh

# After cloning and installing you may want to adjust the chosen user and the chosen environment

# you may want to create a swap file on the remote system
# the following example would create a swap file of 5gb
/home/nvon/nvon-shell-environment/scripts/setup/nwon-se-create-swapfile.sh 5
