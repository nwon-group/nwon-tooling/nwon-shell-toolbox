#!/bin/bash
set -euo pipefail

source $(dirname $(dirname "${BASH_SOURCE[0]}"))/nwon-se-utils.sh

userHasSudoRights

# install Gitlab runner on debian/ubuntu/mint systems
curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash
export GITLAB_RUNNER_DISABLE_SKEL=true
sudo -E apt-get install gitlab-runner

# Download the binary for your system
sudo curl -L --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-386

# Give it permissions to execute
sudo chmod +x /usr/local/bin/gitlab-runner

# Create a GitLab CI user
sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash

# Install and run as service
sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
sudo gitlab-runner start

# register runner
# sudo gitlab-runner register --url https://gitlab.com/ --registration-token Qi3cynqqKmJPP98s1eHn
