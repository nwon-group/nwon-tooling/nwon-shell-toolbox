#!/bin/bash

source ${NWON_SHELL_TOOLBOX_LOCATION}/utils/os/isMacOs.sh

# return current group id dependent on the OS
currentGroupId() {
  if isMacOs; then
    echo "$(id -u)"
  else
    echo "$(id -g)"
  fi
}
