#!/bin/bash

currentUserId() {
  USER_ID=$(id -u)
  echo "${USER_ID}"
}
