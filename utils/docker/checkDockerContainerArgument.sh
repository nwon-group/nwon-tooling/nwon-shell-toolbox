#!/bin/bash

# Checks whether the given container name is valid for a docker-compose
# stack in a given folder

# @param1 directory to look for docker-compose file
# @param2 Chosen container name
# @param3 Additional container names separated by a comma

checkDockerContainerArgument() {
  dockerDir=${1}
  chosenService=${2}
  additionalValidArgument=${3}

  containerList=$(listOfDockerContainers ${dockerDir})

  if [ -n ${additionalValidArgument} ]; then
    containerList="${containerList}, ${additionalValidArgument}"
  fi

  if [ -z "${chosenService}" ]; then
    logError "Please specify to which container you want to connect. Options: ${containerList}"
    exit 1
  fi

  if [[ "${containerList}" != *"${chosenService}"* ]]; then
    logError "${chosenService} is invalid. Please specify a valid container name. Options: ${containerList}"
    exit 1
  fi
}
