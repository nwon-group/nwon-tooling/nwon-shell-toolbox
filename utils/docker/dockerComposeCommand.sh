#!/bin/bash

# @param1 directory to look for docker-compose file
# @param* params to pass to docker-compose

dockerComposeCommand() {
  dockerDirOrComposeFile=${1}

  if [ -z "${dockerDirOrComposeFile}" ]; then
    logError "dockerComposeCommand must be called with a directory or a path to a docker-compose file."
    return 1
  fi

  if [ -f "${dockerDirOrComposeFile}" ]; then
    composeFile=${dockerDirOrComposeFile}
  else
    composeFile=${dockerDirOrComposeFile}/docker-compose.yml
  fi

  if [ ! -f "${composeFile}" ]; then
    logError "Did not find expected ${composeFile}."
    return 1
  fi

  docker-compose -f ${composeFile} ${@:2}
}
