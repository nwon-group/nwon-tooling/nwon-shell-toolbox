#!/bin/bash

# @param1 directory to look for docker-compose file
# @param2 interactive flag
# @param* params to pass to docker-compose

dockerComposeExec() {
  if [ -z "${2}" ]; then
    local interactiveFlag=""
  else
    local interactiveFlag="-T"
  fi

  dockerComposeCommand ${1} exec ${interactiveFlag} ${@:3}
}
