#!/bin/bash

# @param1 directory to look for docker-compose file

listOfDockerContainers() {
  services=$(dockerComposeCommand ${1} ps --services | perl -p -e "s/\n/, /" | perl -p -e "s/, $//")
  echo "${services}"
}
