#!/bin/bash

# check whether the current system is a MacOs

isMacOs() {
  if [[ $(uname -s) = 'Darwin' ]]; then
    return 0
  fi

  return 1
}
