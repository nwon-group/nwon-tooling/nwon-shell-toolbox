#!/bin/bash

# check if the host is running on an m1 processor

isSilicon() {
  if [[ $(uname -p) = 'arm' ]]; then
    return 0
  fi
  return 1
}
